#!/bin/bash

# Set the AWS Region
AWS_REGION="us-east-1"

# Get the task ID of the first task in the ECS service
TASK_ARN=$(aws ecs list-tasks --cluster prod --service-name prod-backend-web --region $AWS_REGION --query 'taskArns[0]' --output text)

if [ "$TASK_ARN" == "None" ]; then
    echo "No tasks found in ECS service."
    exit 1
fi

# Extract the task ID from the task ARN
TASK_ID=$(echo $TASK_ARN | awk -F/ '{print $NF}')

# Execute the command
aws ecs execute-command --task $TASK_ID --command "bash" --interactive --cluster prod --region $AWS_REGION
